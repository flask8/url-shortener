from flask_mail import Message
from app import mail
from flask import current_app, flash


def send_email(to, subject, template):
    msg = Message(subject, recipients=[to], html=template,
                  sender=current_app.config['MAIL_DEFAULT_SENDER'])
    try:
        mail.send(msg)
    except:
        flash('Something went wrong! Try operation later.', 'error')
