import os
import hashlib
import json
import re
import base62
import requests as r
from flask import render_template, redirect, url_for
from app import db
from flask_login import current_user
from app.models import AppInfo, UserEvent, User, Url
from .token import generate_confirmation_token
from .mail import send_email


def generate_url(url):
    urls_count = db.session.query(AppInfo).filter_by(id=1).first()
    string_for_hashing = (url + str(urls_count.urls_counter)).encode('utf-8')
    md5_hash = hashlib.md5(string_for_hashing)
    urls_count.urls_counter += 1
    return base62.encodebytes(md5_hash.digest())[:7]


def add_user_event(event_title, event_desc, c_user):
    user = db.session.query(User).filter_by(id=c_user.id).first()
    if user:
        event = UserEvent(event_title=event_title, event_desc=event_desc)
        user.user_events.append(event)
        db.session.commit()


def get_user_location():
    result = ''
    ip_geoloc_url = 'https://api.ipgeolocation.io/ipgeo?apiKey='
    key = os.getenv('IPGEOLOC_API_KEY')
    try:
        response = r.get(ip_geoloc_url + key).text
        data = json.loads(response)
        result = 'from {ip} ({org}) ({country}, {district}, {city})'.format(
           ip=data['ip'], org=data['organization'], country=data['country_name'], district=data['district'],
           city=data['city']
        )
    except Exception as e:
        print(e)
    return result


def email_existing(email):
    return bool(db.session.query(User).filter_by(email=email).first())


def check_email(email):
    regex = '^[a-z0-9]+[\._]?[a-z0-9]+[@]\w+[.]\w{2,3}$'
    return True if re.fullmatch(regex, email) else False


def check_name(name):
    return all(x.isalpha() or x.isspace() for x in name)


def check_confirmed():
    if not current_user.confirmed:
        return redirect(url_for('auth.unconfirmed'))


def generate_confirm_letter(email, c_app, subject='Please confitm your email!'):
    token = generate_confirmation_token(email, c_app)
    confirm_url = url_for('auth.confirm_email', token=token, _external=True)
    html = render_template('auth/activate_message.html', confirm_url=confirm_url)
    send_email(email, subject, html)


def lock_url(c_user):
    if not c_user.user_info.premium:
        locked_urls_count = 0
        last_5_urls = db.session.query(Url.id).order_by(db.desc(Url.created_on)). \
            filter_by(user_id=c_user.id, custom=False).limit(5)
        for row in db.session.query(Url).filter(~Url.id.in_(last_5_urls)):
            if not row.locked:
                locked_urls_count += 1
                row.locked = True
        for row in db.session.query(Url).filter(Url.id.in_(last_5_urls)):
            if row.locked:
                locked_urls_count -= 1
                row.locked = False
        c_user.user_info.urls_count -= locked_urls_count
    else:
        unlocked_urls_count = 0
        for row in db.session.query(Url).filter_by(locked=True, user_id=c_user.id):
            row.locked = False
            unlocked_urls_count += 1
        c_user.user_info.urls_count += unlocked_urls_count
    db.session.commit()
