from ..main import main


@main.app_template_filter()
def tags_str_to_list(tag_str):
    tag_list = tag_str.split(',')
    return list(filter(None, [x.strip() for x in tag_list]))
