from app import db
from flask_login import UserMixin


class Base(db.Model):
    __abstract__ = True
    updated_on = db.Column(db.DateTime, default=db.func.now(), onupdate=db.func.now())
    created_on = db.Column(db.DateTime, default=db.func.now())


class User(UserMixin, Base):
    __tablename__ = 'users'
    id = db.Column(db.Integer, primary_key=True)
    email = db.Column(db.String(100), nullable=False, unique=True)
    password = db.Column(db.String(200), nullable=False)
    name = db.Column(db.String(1000), nullable=False)
    confirmed = db.Column(db.Boolean, default=False)
    urls = db.relationship('Url', backref='user', lazy=True, cascade='all,delete')
    user_events = db.relationship('UserEvent', backref='user', lazy=True, cascade='all,delete')
    user_info = db.relationship('UserInfo', backref='user', lazy=True, uselist=False, cascade='all,delete')

    def __init__(self, email, password, name, confirmed):
        self.email = email
        self.password = password
        self. name = name
        self.confirmed = confirmed

    def __repr__(self):
        return f'<User: {self.name}>'


class UserInfo(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    admin = db.Column(db.Boolean, default=False)
    premium = db.Column(db.Boolean, default=False)
    premium_expires = db.Column(db.DateTime)
    urls_count = db.Column(db.Integer, default=0)
    user_id = db.Column(db.Integer, db.ForeignKey('users.id'), nullable=False)

    def __repr__(self):
        return f'<UserStatus: {self.user_id}>'


class Url(Base):
    __tablename__ = 'urls'
    id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.String(200))
    url = db.Column(db.Text, nullable=False, unique=True)
    original_url = db.Column(db.Text, nullable=False)
    tags = db.Column(db.Text)
    custom = db.Column(db.Boolean, default=False)
    visits = db.Column(db.Integer)
    locked = db.Column(db.Boolean, default=False)
    user_id = db.Column(db.Integer, db.ForeignKey('users.id'))

    def __init__(self,  url, original_url, visits=0):
        self.url = url
        self.original_url = original_url
        self.visits = visits

    def __repr__(self):
        return f'<Url: {self.url}>'


class AppInfo(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    urls_counter = db.Column(db.BigInteger)

    def __init__(self, urls_counter=0):
        self.urls_counter = urls_counter


class UserEvent(Base):
    __tablename__ = 'userevents'
    id = db.Column(db.Integer, primary_key=True)
    event_title = db.Column(db.String(200))
    event_desc = db.Column(db.Text)
    user_id = db.Column(db.Integer, db.ForeignKey('users.id'), nullable=False)

    def __init__(self, event_title, event_desc):
        self.event_title = event_title
        self.event_desc = event_desc

    def __repr__(self):
        return f'<UserEvent: {self.event_title}>'
