  $(document).ready(function () {
      $("#btn-prem").click(function( event ) {
        let btn_value = this.value;
        let user_id = $('#user-id').val();
        activate( user_id, btn_value);
        event.preventDefault();
      });
  });


function activate(id, btn_value) {
    let form = $('#activate-prem-form')
    $.ajax({
        type: form.attr('method'),
        url: form.attr('action'),
        data: form.serialize(),
        success: function (response) {
            if (btn_value === 'Active'){
                $('#btn-prem').addClass('btn-warning').removeClass('btn-success').val('Activate');
            }
            if (btn_value === 'Activate'){
                $('#btn-prem').addClass('btn-success').removeClass('btn-warning').val('Active');
            }
        },
        error: function (error) {
            console.log(error);
        }
    });
}