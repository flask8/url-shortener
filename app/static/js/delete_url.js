  $(document).ready(function () {
      $("[id^='btn-delete-']" ).click(function( event ) {
        let id = event.target.id;
        let id_start_pos = id.lastIndexOf('-');
        let url_id = id.slice(id_start_pos+1);
        console.log(url_id);
        deleteUrl( url_id);
        event.preventDefault();
      });
  });


function deleteUrl(id) {
    let form = $('#form-del-' + id);
    $.ajax({
        type: form.attr('method'),
        url: form.attr('action'),
        data: form.serialize(),
        success: function (response) {
            $("li#li-"+id).remove();
            },
            error: function (error) {
                console.log(error);
            }
        });
    }
