  $(document).ready(function () {
      $("[id^='btn-update-']" ).click(function( event ) {
          console.log(this.value);
        let id = event.target.id;
        console.log(id);
        let id_start_pos = id.lastIndexOf('-');
        let url_id = id.slice(id_start_pos+1);
        updateUrl( url_id);
        event.preventDefault();
      });
  });


function updateUrl(id) {
            let form = $('#form-' + id)
            $.ajax({
                type: form.attr('method'),
                url: form.attr('action'),
                data: form.serialize(),
                success: function (response) {
                    console.log(response['msg']);
                    if (response['msg']){
                        alert(response['msg']);
                        let err_status = true;
                        spawnTempMsg(id, err_status, response['msg']);
                        return;
                    }else{
                        let err_status=false;
                        spawnTempMsg(id, err_status);
                    }

                    title_p = $("li#li-"+id).find('div > p.title');
                    title_p.eq(0).text(response.title);
                    title_length = title_p.length;
                    $("li#li-"+id).find('div > h3').text(response.url);
                    $("li#li-"+id).find('div > p.orig-url').eq(0).text(response.original_url);
                    let tags_string = response.tags;
                    let tags = tags_string.split(',');
                    let filtered = tags.filter(Boolean);
                    tags_space = $("li#li-"+id).find('div#tags-'+id);
                    $.each(filtered, function (k,v){
                        if (k === 0){ tags_space.empty(); }
                        tags_space.append('<a href=\"/index?search_field=23ads\"><span ' +
                            'class=\"badge bg-dark\">' + $.trim(v) +'</span></a> ');
                    });
                },
                error: function (error) {
                    console.log(error);
                }
            });
        }

function spawnTempMsg(id, err_status, resp=null) {
    let alert_msg = ''
    if (err_status === true){
        alert_msg = $("<div class=\"alert alert-danger d-flex align-items-center\" role=\"alert\">\n" +
        "  <svg class=\"bi flex-shrink-0 me-2\" width=\"24\" height=\"20\" role=\"img\" aria-label=\"Success:\">" +
        "<use xlink:href=\"#check-circle-fill\"/></svg>\n" +
        "  <div>\n" +
        "    " + resp + "\n" +
        "  </div>\n" +
        "</div>").appendTo("#ajax-message-" + id);
    }else{
        alert_msg = $("<div class=\"alert alert-success d-flex align-items-center\" role=\"alert\">\n" +
        "  <svg class=\"bi flex-shrink-0 me-2\" width=\"24\" height=\"20\" role=\"img\" aria-label=\"Success:\">" +
        "<use xlink:href=\"#check-circle-fill\"/></svg>\n" +
        "  <div>\n" +
        "    Updated\n" +
        "  </div>\n" +
        "</div>").appendTo("#ajax-message-" + id);
    }

  setTimeout(function() {
    alert_msg.remove();
  }, 3500);
}