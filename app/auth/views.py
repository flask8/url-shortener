from flask import render_template, redirect, url_for, flash, request, current_app
from flask_login import login_user, login_required, logout_user, current_user
from werkzeug.security import generate_password_hash, check_password_hash

from app import db
from app.models import User, UserInfo
from . import auth
from .forms import LoginForm, RegisterForm
from app.scripts.main_func import add_user_event, get_user_location, check_email, check_confirmed
from app.scripts.token import generate_confirmation_token, confirm_token
from app.scripts.mail import send_email


@auth.route('/login', methods=['POST', 'GET'])
def login():
    form = LoginForm()
    if form.validate_on_submit():
        email = form.email.data
        password = form.password.data
        remember = bool(form.remember.data)
        user = db.session.query(User).filter_by(email=email).first()
        if not user or not check_password_hash(user.password, password):
            flash("Wrong password or user doesn't exist!")
            return redirect(url_for('auth.login'))
        if login_user(user, remember=remember):
            add_user_event('Log In', get_user_location(), current_user)
        check_confirmed()
        return redirect(url_for('main.index'))
    return render_template('auth/login.html', form=form)


@auth.route('/signup', methods=['POST', 'GET'])
def signup():
    form = RegisterForm()
    if form.validate_on_submit():
        email = form.email.data
        name = form.name.data
        password = form.password.data
        user = db.session.query(User).filter_by(email=email).first()
        if user:
            flash('The email address is already taken. Please choose another', 'warning')
            return render_template('auth/register.html', form=form)
        if not check_email(email):
            flash('Incorrect email format', 'warning')
            return render_template('auth/register.html', form=form)
        new_user = User(email=email, password=generate_password_hash(password), name=name, confirmed=False)
        new_user_stats = UserInfo(user=new_user)
        db.session.add(new_user)
        db.session.add(new_user_stats)
        db.session.commit()
        token = generate_confirmation_token(new_user.email, current_app)
        confirm_url = url_for('auth.confirm_email', token=token, _external=True)
        html = render_template('auth/activate_message.html', confirm_url=confirm_url)
        subject = 'Please confitm your email!'
        send_email(new_user.email, subject, html)
        if login_user(new_user, remember=True):
            add_user_event('Account created', get_user_location(), current_user)
            add_user_event('Log In', get_user_location(), current_user)
        flash('A confirmation email has been sent via email', 'success')
        return redirect(url_for('auth.unconfirmed'))
    return render_template('auth/register.html', form=form)


@auth.route('/logout')
@login_required
def logout():
    logout_user()
    return redirect(url_for('main.index'))


@auth.route('/confirm/<token>')
@login_required
def confirm_email(token):
    email = ''
    try:
        email = confirm_token(token, current_app)
    except:
        flash('The confirmation link is invalid or has expired', 'danger')
    if not email:
        flash('The confirmation link is invalid or has expired', 'danger')
        return redirect(url_for('auth.unconfirmed'))
    user = db.session.query(User).filter_by(email=email).first_or_404()
    if user.confirmed:
        flash('Account already confirmed.', 'success')
    else:
        user.confirmed = True
        db.session.add(user)
        db.session.commit()
        flash('You have confirmed you account. Thanks!', 'success')
    return redirect(url_for('main.index'))


@auth.route('/unconfirmed')
@login_required
def unconfirmed():
    if current_user.confirmed:
        return redirect(url_for('main.index'))
    flash('Please confirm your account!', 'warning')
    return render_template('auth/unconfirmed.html')


@auth.route('/resend')
@login_required
def resend():
    token = generate_confirmation_token(current_user.email, current_app)
    confirm_url = url_for('auth.confirm_email', token=token, _external=True)
    html = render_template('auth/activate_message.html', confirm_url=confirm_url)
    subject = 'Please confirm your email'
    send_email(current_user.email, subject, html)
    flash('A new confirmation email has been sent', 'success')
    return redirect(url_for('auth.unconfirmed'))
