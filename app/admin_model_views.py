from flask_admin.contrib.sqla import ModelView
from flask_admin import AdminIndexView
from flask_login import current_user
from flask import redirect, url_for


class CustomAdminIndexView(AdminIndexView):
    def is_accessible(self):
        if current_user.is_authenticated and not current_user.is_anonymous:
            return current_user.user_info.admin
        return False

    def inaccessible_callback(self, name, **kwargs):
        return redirect(url_for('main.index'))


class BaseModelView(ModelView):
    def is_accessible(self):
        if current_user.is_authenticated and not current_user.is_anonymous:
            return current_user.user_info.admin
        return False

    def inaccessible_callback(self, name, **kwargs):
        return redirect(url_for('main.index'))


class UserModelView(BaseModelView):
    column_searchable_list = ['email', 'name']
    column_filters = ['confirmed']
    page_size = 50
    # without created_on and pass columns
    column_list = ['id', 'updated_on', 'email', 'name', 'confirmed']


class UserInfoModelView(BaseModelView):
    column_searchable_list = ['user.name']
    column_filters = ['admin', 'premium']
    column_list = ['id', 'admin', 'premium', 'premium_expires', 'urls_count', 'user.name']


class UrlModelView(BaseModelView):
    column_searchable_list = ['user.name', 'tags', 'title', 'url', 'original_url']
    column_filters = ['custom']
    column_list = ['id', 'original_url', 'url', 'title', 'tags', 'custom', 'locked', 'visits', 'user.name']


class AppInfoModelView(BaseModelView):
    pass


class UserEventModelView(ModelView):
    pass
