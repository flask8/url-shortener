import os
from flask import Flask
from flask_bootstrap import Bootstrap5
from flask_login import LoginManager
from flask_mail import Mail
from flask_sqlalchemy import SQLAlchemy
from flask_admin import Admin
from flask_admin.contrib.sqla import ModelView
from .admin_model_views import UserModelView, UserInfoModelView, CustomAdminIndexView, UrlModelView, AppInfoModelView, \
    UserEventModelView

db = SQLAlchemy()
bootstrap = Bootstrap5()
mail = Mail()
login_manager = LoginManager()
admin = Admin(name='Url shotener db', template_mode='bootstrap3', index_view=CustomAdminIndexView())


def create_app():
    app = Flask(__name__)
    app.config.from_object(os.environ['APP_SETTINGS'])

    register_blueprints(app)
    initialize_extensions(app)
    from .models import User, AppInfo, UserInfo, Url, UserEvent

    admin.add_view(UserModelView(User, db.session))
    admin.add_view(UrlModelView(Url, db.session,  endpoint="urls"))
    admin.add_view(UserInfoModelView(UserInfo, db.session, endpoint="user_info"))
    admin.add_view(UserEventModelView(UserEvent, db.session,  endpoint="events"))
    admin.add_view(AppInfoModelView(AppInfo, db.session))

    @login_manager.user_loader
    def load_user(user_id):
        print('loader:',  User.query.get(int(user_id)))
        return User.query.get(int(user_id))

    with app.app_context():
        db.create_all()
        if not db.session.query(AppInfo).filter_by(id=1).first():
            appinf = AppInfo(urls_counter=0)
            db.session.add(appinf)
            db.session.commit()

    return app


def register_blueprints(app):
    from app.main import main
    from app.auth import auth
    app.register_blueprint(main)
    app.register_blueprint(auth)


def initialize_extensions(app):
    db.init_app(app)
    bootstrap.init_app(app)
    mail.init_app(app)
    admin.init_app(app)

    login_manager.login_view = 'auth.login'
    login_manager.init_app(app)



