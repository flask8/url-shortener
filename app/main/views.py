import os
from flask import render_template, redirect, url_for, request, flash, Markup, session, jsonify, current_app, abort
from flask_login import login_required, current_user
from werkzeug.security import check_password_hash, generate_password_hash
from app import db
from . import main
from .forms import CreateUrlForm, UpdateUserSettingsForm, ActivatePremiumForm, SearchUrlForm, UpdateUrlForm, DeleteForm, ActivatePremiumForm
from ..models import Url, User, AppInfo, UserEvent, UserInfo
from app.scripts.main_func import generate_url, check_email, check_name, check_confirmed, email_existing, \
    generate_confirm_letter, lock_url
from app.scripts.token import generate_confirmation_token
from app.scripts.custom_filters import tags_str_to_list


@main.route('/', methods=['GET', 'POST'])
@main.route('/index', methods=['GET', 'POST'])
@main.route('/index/<int:page>', methods=['GET', 'POST'])
def index(page=1):
    if current_user.is_authenticated:
        #lc_test(current_user)
        lock_url(current_user)
    generate_confirmation_token('ilya.khotk@gmail.com', current_app)
    session['search'] = ''
    search_form = SearchUrlForm()
    update_form = UpdateUrlForm()
    delete_form = DeleteForm()
    check_auth = current_user.is_authenticated
    search_data = request.args.get('search_field')
    per_page = 5
    if check_auth:
        if not current_user.confirmed:
            return redirect(url_for('auth.unconfirmed'))
        if search_data:
            if session['search'] != search_data:
                page = 1
            session['search'] = search_data
            urls = Url.query.filter(Url.original_url.contains(search_data) |
                                    Url.url.contains(search_data) |
                                    Url.title.contains(search_data) |
                                    Url.tags.contains(search_data)).\
                order_by(db.desc(Url.id)).paginate(page, per_page, error_out=False)
        else:
            if session['search']:
                page = 1
                session['search'] = ''
            urls = db.session.query(Url).filter_by(user_id=current_user.id).order_by(db.desc(Url.id)).paginate(
                page, per_page, error_out=False)
        return render_template('main/index.html', urls=urls, user=current_user, form=search_form,
                               check_auth=check_auth, search_flag=session['search'], search_data=search_data,
                               update_form=update_form, delete_form=delete_form)
    return render_template('main/index.html', check_auth=check_auth)


@main.route('/settings', methods=['GET', 'POST'])
@login_required
def settings():
    check_confirmed()
    lock_url(current_user)
    form = UpdateUserSettingsForm()
    premium_form_act = ActivatePremiumForm()
    check_premium = db.session.query(UserInfo).filter_by(user_id=current_user.id).first()
    premium_form_act.submit.label.text = 'Active' if check_premium.premium else 'Activate'
    if request.method == 'POST':
        if form.validate_on_submit():
            user = db.session.query(User).filter_by(id=current_user.id).first()
            old_pass = form.current_password.data
            new_pass = form.change_password.data
            new_pass_confirm = form.confirm_password.data
            flash_success_string = ''
            if old_pass:
                if check_password_hash(current_user.password, old_pass):
                    if new_pass and new_pass_confirm:
                        if new_pass == new_pass_confirm:
                            user.password = generate_password_hash(new_pass)
                            flash_success_string += '<p>Password was changed!<p>'
                        else:
                            flash('New password and confirm password not equal!')
                    else:
                        flash('To change password you must fill all password fields')
                else:
                    flash('Old password is wrong!')
            new_email = form.change_email.data
            if new_email and new_email != user.email:
                if not email_existing(new_email):
                    if check_email(new_email):
                        user.email = new_email
                        flash_success_string += '<p>Email was changed!</p>'
                        user.confirmed = False
                        generate_confirm_letter(new_email, current_app)
                    else:
                        flash('Incorrect email format', 'warning')
                else:
                    flash('The email address is already taken. Please choose another', 'warning')
            new_name = form.change_name.data
            if new_name and new_name != user.name:
                if check_name(new_name):
                    user.name = new_name
                    flash_success_string += '<p>Name was changed!</p>'
                else:
                    flash('User name must contain only letters and spaces')
            if flash_success_string:
                flash(Markup(flash_success_string))

            db.session.commit()
        return redirect(url_for('main.settings'))
    events = db.session.query(UserEvent).filter_by(user_id=current_user.id).order_by(db.desc(UserEvent.id)).limit(10).all()
    return render_template("main/settings.html", c_user=current_user, form=form, events=events,
                           premium_form=premium_form_act)


@main.route('/create_url', methods=['GET', 'POST'])
@login_required
def create_url():
    """Create a new short url. If user has premium,
    he can create his own custom url (if this url doesn't exist in db)."""
    check_confirmed()
    form = CreateUrlForm()
    main_url = os.getenv('SITE_URL')
    premium = current_user.user_info.premium
    if form.validate_on_submit():
        c_user_info = db.session.query(UserInfo).filter_by(user_id=current_user.id).first()
        url_limit = True if int(c_user_info.urls_count) >= 5 and not premium else False
        if url_limit:
            flash(Markup('You can create only 5 urls!<br>Buy premium for more'))
            return render_template('main/create_url.html', form=form, premium=premium, main_url=main_url)
        url = form.original_url.data
        custom = form.custom_field.data
        title = form.title.data
        tags = form.tags.data
        if url.startswith(main_url):
            flash(f"You can't use this url {main_url}!", 'warning')
            return render_template('main/create_url.html', form=form, premium=premium, main_url=main_url)
        custom_flag = False
        if url:
            if custom:
                if premium:
                    custom = main_url + custom
                    check_custom_url = bool(db.session.query(Url).filter_by(url=custom).first())
                    if check_custom_url:
                        flash('This(custom) url already exists')
                        return render_template('main/create_url.html', form=form, premium=premium, main_url=main_url)
                    custom_flag = True
                else:
                    flash('You should activate premium')
                    return render_template('main/create_url.html', form=form, premium=premium, main_url=main_url)
            else:
                custom = main_url + generate_url(url)[:7]
            new_url = Url(url=custom, original_url=url)
            new_url.custom = custom_flag
            if title:
                new_url.title = title
            if tags:
                new_url.tags = tags
            c_user = db.session.query(User).filter_by(id=current_user.id).first()
            c_user.urls.append(new_url)
            c_user_info.urls_count += 1
            db.session.commit()
            #usr = db.session.query(UserInfo).filter_by(user_id=current_user.id).first()
        return redirect(url_for('main.index'))

    return render_template('main/create_url.html', form=form, premium=premium, main_url=main_url)


@main.route('/activate_premium', methods=['POST'])
@login_required
def activate_premium():
    """Activate premium account"""
    form = ActivatePremiumForm()
    if form.validate_on_submit():
        hidden_usr = form.hidden_user_id.data
        upd_user, prem = db.session.query(UserInfo, UserInfo.premium).filter_by(user_id=hidden_usr).first()
        if upd_user:
            upd_user.premium = not prem
        db.session.commit()
        return jsonify({'success': 'success'})


@main.route('/update_url', methods=['POST'])
@login_required
def update_url():
    check_confirmed()
    u_form = UpdateUrlForm()
    original_url = u_form.original_url.data
    custom_url = u_form.custom_field.data
    title = u_form.title.data
    tags = u_form.tags.data
    url_id = u_form.hidden_url_id.data
    main_url = os.getenv('SITE_URL')
    if original_url.startswith(main_url):
        return jsonify({'msg': f"You can't use this url {main_url}"})
    users_premium = db.session.query(UserInfo.premium).filter_by(user_id=current_user.id).first()
    if u_form.validate_on_submit():
        if users_premium[0]:
            url_exists = bool(db.session.query(Url).filter_by(url=custom_url).first())
            owner_cheking = bool(db.session.query(Url).filter_by(url=custom_url, id=url_id, user_id=current_user.id).first())
            if not url_exists or bool(owner_cheking):
                db.session.query(Url).filter_by(id=url_id).update({'original_url': original_url, 'url': custom_url,
                                                                   'title': title, 'tags': tags})
            else:
                print('Already exists!')
                return jsonify({'msg': 'This(custom) url already exists'})
        else:
            print('HERE')
            db.session.query(Url).filter_by(id=url_id).update({'original_url': original_url,
                                                               'title': title, 'tags': tags})
        db.session.commit()
    print(original_url, custom_url, title, tags)
    return jsonify({'original_url': original_url, 'url': custom_url,
                    'title': title, 'tags': tags})


@main.route('/delete_url', methods=['POST'])
@login_required
def delete_url():
    del_form = DeleteForm()
    url_id = del_form.hidden_url_id.data
    url_usr = db.session.query(Url, UserInfo).filter(Url.user_id == UserInfo.user_id).\
        filter(Url.id == url_id, Url.user_id == current_user.id).first()
    db.session.delete(url_usr[0])
    if not url_usr[1].premium and url_usr[0].locked:
        pass
    else:
        url_usr[1].urls_count -= 1
    db.session.commit()
    flash('Url was deleted successful!', 'success')
    return jsonify({'success': url_id})


@main.route('/<string:token>', methods=['GET'])
def url_redirect(token):
    main_url_len = os.getenv('SITE_URL')
    searching_string = main_url_len + str(token)
    needle_url = db.session.query(Url).filter(Url.url == searching_string).first()
    if needle_url:
        if not needle_url.locked:
            return redirect(needle_url.original_url)
    abort(404)


@main.route('/repo')
def repo_redirect():
    return redirect('https://gitlab.com/flask8/url-shortener')


@main.errorhandler(404)
def page_not_found(e):
    return render_template('main/errors/404.html'), 404
