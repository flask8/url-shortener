from flask_wtf import FlaskForm
from wtforms import URLField, SubmitField, StringField, PasswordField, EmailField, HiddenField
from wtforms.validators import DataRequired, EqualTo


class CreateUrlForm(FlaskForm):
    title = StringField('Enter title')
    original_url = URLField('Enter url', validators=[DataRequired()])
    custom_field = StringField('Custom link')
    tags = StringField('Tags (Separate tags with commas)')
    submit = SubmitField('Create')


class UpdateUserSettingsForm(FlaskForm):
    current_password = PasswordField('Current password')
    change_password = PasswordField('New password')
    confirm_password = PasswordField('Confirm new password',
                                     validators=[EqualTo('change_password',
                                                         message='Both password fields must be equal')])
    change_email = EmailField('Email')
    change_name = StringField('Full name')
    submit = SubmitField('Save')


class ActivatePremiumForm(FlaskForm):
    hidden_user_id = HiddenField('user')
    submit = SubmitField('Activate')


class SearchUrlForm(FlaskForm):
    search_field = StringField('Enter something')
    submit = SubmitField('Search')


class DeleteForm(FlaskForm):
    hidden_url_id = HiddenField('url_id')
    delete = SubmitField('Delete')


class UpdateUrlForm(CreateUrlForm):
    hidden_url_id = HiddenField('url_id')
    submit = SubmitField('Update')
