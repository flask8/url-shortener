FROM python:3.10
MAINTAINER 'khotk.ilya@gmail.com'

ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1

COPY . /app
WORKDIR /app
RUN /usr/local/bin/python -m pip install --upgrade pip &&\
    pip install --no-cache-dir -r requirements.txt
ENTRYPOINT ["python"]
CMD ["wsgi.py"]